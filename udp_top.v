`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:24:35 11/18/2017 
// Design Name: 
// Module Name:    udp_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module udp_top(
    output rgmii_txc_1,
    output [3:0] rgmii_txd_1,
	 output rgmii_tx_ctl_1,
	 output led_2,
	 input system_clk_p,
	 input system_clk_n,
	 output phy_rstn_1
    );
	 

wire clk_25shifted;
wire clk_25;
assign phy_rstn_1=1'b1;

CLK50 clk50 (.CLK_IN1_P(system_clk_p),.CLK_IN1_N(system_clk_n),.CLK_OUT1(clk_25),.CLK_OUT2(clk_25shifted));
	 
reg     [31:0] crc = 0;
  
  function [31:0] crc_table;
    input [3:0] addr;
    case (addr)
       0: crc_table = 32'h4DBDF21C;
       1: crc_table = 32'h500AE278;
       2: crc_table = 32'h76D3D2D4;
       3: crc_table = 32'h6B64C2B0;
       4: crc_table = 32'h3B61B38C;
       5: crc_table = 32'h26D6A3E8;
       6: crc_table = 32'h000F9344;
       7: crc_table = 32'h1DB88320;
       8: crc_table = 32'hA005713C;
       9: crc_table = 32'hBDB26158;
      10: crc_table = 32'h9B6B51F4;
      11: crc_table = 32'h86DC4190;
      12: crc_table = 32'hD6D930AC;
      13: crc_table = 32'hCB6E20C8;
      14: crc_table = 32'hEDB71064;
      15: crc_table = 32'hF0000000;
      default: crc_table = 32'bx;
    endcase
  endfunction
	 
// "IP source" - put an unused IP - if unsure, see comment below after the source code
parameter IPsource_1 = 169;
parameter IPsource_2 = 254;
parameter IPsource_3 = 99;
parameter IPsource_4 = 44;

// "IP destination" - put the IP of the PC you want to send to
parameter IPdestination_1 = 8'hff;
parameter IPdestination_2 = 8'hff;
parameter IPdestination_3 = 8'hff;
parameter IPdestination_4 = 8'hff;

// "Physical Address" - put the address of the PC you want to send to
parameter PhysicalAddress_1 = 8'hff;
parameter PhysicalAddress_2 = 8'hff;
parameter PhysicalAddress_3 = 8'hff;
parameter PhysicalAddress_4 = 8'hff;
parameter PhysicalAddress_5 = 8'hff;
parameter PhysicalAddress_6 = 8'hff;

// calculate the IP checksum, big-endian style
parameter IPchecksum1 = 32'h0000C53F + (IPsource_1<<8)+IPsource_2+(IPsource_3<<8)+IPsource_4+(IPdestination_1<<8)+IPdestination_2+(IPdestination_3<<8)+(IPdestination_4);
parameter IPchecksum2 =  ((IPchecksum1&32'h0000FFFF)+(IPchecksum1>>16));
parameter IPchecksum3 = ~((IPchecksum2&32'h0000FFFF)+(IPchecksum2>>16));

//////////////////////////////////////////////////////////////////////
// sends a packet roughly every second

`ifdef XILINX_ISIM
	reg [8:0] counter=9'b111110111; 
`else
	reg [26:0] counter;
`endif

always @(posedge clk_25) counter<=counter+1;
reg StartSending; always @(posedge clk_25) StartSending<=&counter;

assign led_2=counter>=31'd120000000;
		

reg [6:0] rdaddress=7'd0;
reg [7:0] pkt_data;

always @(posedge clk_25) 
case(rdaddress)
// Ethernet preamble
  7'h00: pkt_data <= 8'h55;
  7'h01: pkt_data <= 8'h55;
  7'h02: pkt_data <= 8'h55;
  7'h03: pkt_data <= 8'h55;
  7'h04: pkt_data <= 8'h55;
  7'h05: pkt_data <= 8'h55;
  7'h06: pkt_data <= 8'h55;
  7'h07: pkt_data <= 8'hD5;
// Ethernet header
  7'h08: pkt_data <= PhysicalAddress_1;
  7'h09: pkt_data <= PhysicalAddress_2;
  7'h0A: pkt_data <= PhysicalAddress_3;
  7'h0B: pkt_data <= PhysicalAddress_4;
  7'h0C: pkt_data <= PhysicalAddress_5;
  7'h0D: pkt_data <= PhysicalAddress_6;
  7'h0E: pkt_data <= 8'h00;
  7'h0F: pkt_data <= 8'h12;
  7'h10: pkt_data <= 8'h34;
  7'h11: pkt_data <= 8'h56;
  7'h12: pkt_data <= 8'h78;
  7'h13: pkt_data <= 8'h90;
// IP header
  7'h14: pkt_data <= 8'h08;
  7'h15: pkt_data <= 8'h00;
  7'h16: pkt_data <= 8'h45;
  7'h17: pkt_data <= 8'h00;
  7'h18: pkt_data <= 8'h00;
  7'h19: pkt_data <= 8'h2E;
  7'h1A: pkt_data <= 8'h00;
  7'h1B: pkt_data <= 8'h00;
  7'h1C: pkt_data <= 8'h00;
  7'h1D: pkt_data <= 8'h00;
  7'h1E: pkt_data <= 8'h80;
  7'h1F: pkt_data <= 8'h11;
  7'h20: pkt_data <= IPchecksum3[15:8];
  7'h21: pkt_data <= IPchecksum3[ 7:0];
  7'h22: pkt_data <= IPsource_1;
  7'h23: pkt_data <= IPsource_2;
  7'h24: pkt_data <= IPsource_3;
  7'h25: pkt_data <= IPsource_4;
  7'h26: pkt_data <= IPdestination_1;
  7'h27: pkt_data <= IPdestination_2;
  7'h28: pkt_data <= IPdestination_3;
  7'h29: pkt_data <= IPdestination_4;
// UDP header
  7'h2A: pkt_data <= 8'h04;
  7'h2B: pkt_data <= 8'h00;
  7'h2C: pkt_data <= 8'h04;
  7'h2D: pkt_data <= 8'h00;
  7'h2E: pkt_data <= 8'h00;
  7'h2F: pkt_data <= 8'h1A;
  7'h30: pkt_data <= 8'h00;
  7'h31: pkt_data <= 8'h00;
// payload
  7'h32: pkt_data <= 8'h00;	// put here the data that you want to send
  7'h33: pkt_data <= 8'h01;	// put here the data that you want to send
  7'h34: pkt_data <= 8'h02;	// put here the data that you want to send
  7'h35: pkt_data <= 8'h03;	// put here the data that you want to send
  7'h36: pkt_data <= 8'h04;	// put here the data that you want to send
  7'h37: pkt_data <= 8'h05;	// put here the data that you want to send
  7'h38: pkt_data <= 8'h06;	// put here the data that you want to send
  7'h39: pkt_data <= 8'h07;	// put here the data that you want to send
  7'h3A: pkt_data <= 8'h08;	// put here the data that you want to send
  7'h3B: pkt_data <= 8'h09;	// put here the data that you want to send
  7'h3C: pkt_data <= 8'h0A;	// put here the data that you want to send
  7'h3D: pkt_data <= 8'h0B;	// put here the data that you want to send
  7'h3E: pkt_data <= 8'h0C;	// put here the data that you want to send
  7'h3F: pkt_data <= 8'h0D;	// put here the data that you want to send
  7'h40: pkt_data <= 8'h0E;	// put here the data that you want to send
  7'h41: pkt_data <= 8'h0F;	// put here the data that you want to send
  7'h42: pkt_data <= 8'h10;	// put here the data that you want to send
  7'h43: pkt_data <= 8'h11;	// put here the data that you want to send
  default: pkt_data <= 8'h00;
endcase

reg  ShiftCount=1;
reg SendingPacket=0;
reg tx_en=0;


always @(posedge clk_25) if(StartSending) SendingPacket<=1; else if(rdaddress==7'h48) SendingPacket<=0;

always @(posedge clk_25) ShiftCount <= SendingPacket ? ShiftCount+1 : 1'd1;

always @(posedge clk_25) tx_en<=SendingPacket;

assign rgmii_tx_ctl_1 = tx_en;

wire readram = (ShiftCount==1'd1);

always @(posedge clk_25) if(ShiftCount==1) rdaddress <= SendingPacket ? rdaddress+1 : 7'b0;

reg [7:0] ShiftData; always @(posedge clk_25) ShiftData <= readram ? pkt_data : {4'b0, ShiftData[7:4]};

reg CRCflush; always @(posedge clk_25) if(CRCflush) CRCflush <= SendingPacket; else if(readram) CRCflush <= (rdaddress==7'h44);

reg CRCinit; always @(posedge clk_25) if(readram) CRCinit <= (rdaddress==7);

always @(posedge clk_25)
begin
	if (CRCinit)
		crc<=0;
	else if (CRCflush)
		crc<={4'b0,crc[31:4]};
	else
		crc<=crc[31:4]  ^ crc_table(crc[3:0]  ^ ShiftData[3:0]);
end

wire[3:0] dataout; 


assign dataout[0]=CRCflush ? crc[0] : ShiftData[0];
assign dataout[1]=CRCflush ? crc[1] : ShiftData[1];
assign dataout[2]=CRCflush ? crc[2] : ShiftData[2];
assign dataout[3]=CRCflush ? crc[3] : ShiftData[3];


reg tx_clk=0;

	
always @(posedge clk_25shifted)
	tx_clk<=tx_clk?0:1;

assign rgmii_txc_1 = tx_clk;
	

assign rgmii_txd_1[3] =  dataout[3] ;
assign rgmii_txd_1[2] =  dataout[2] ;
assign rgmii_txd_1[1] =  dataout[1] ;
assign rgmii_txd_1[0] =  dataout[0]  ;

endmodule
