
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name udp_test -dir "C:/Users/User/udp_test/planAhead_run_1" -part xc7k325tffg676-1
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/User/udp_test/udp_top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/User/udp_test} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "netfpga.ucf" [current_fileset -constrset]
add_files [list {netfpga.ucf}] -fileset [get_property constrset [current_run]]
link_design
